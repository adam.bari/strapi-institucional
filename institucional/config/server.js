module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  // url: 'https://strapi.institucional.bancobari.com.br',
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'what you hear? nothing but the rain'),
    },
  },
})
