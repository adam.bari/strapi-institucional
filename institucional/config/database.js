module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', 'localhost'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'strapi'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', 'strapi'),
      },
      options: {
        debug: true,
        pool: {
          min: 0,
          max: 30,
          acquireTimeoutMillis: 30000,
          createTimeoutMillis: 1500,
          idleTimeoutMillis: 100,
          createRetryIntervalMillis: 500,
          // propagateCreateError: false
        },
      },
    },
  },
})
