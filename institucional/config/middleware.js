module.exports = {
  settings: {
    cors: {
      enabled: true,
      origin: '*',
      method: ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "HEAD"]
    },
  },
}
