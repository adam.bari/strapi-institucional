FROM ubuntu:18.04

ENV NODE_ENV=production
ENV PATH /app/node_modules/.bin:$PATH
# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN mkdir /app
COPY ./ /app/

# Set debconf to run non-interactively
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install base dependencies
RUN apt-get update && apt-get install -y -q --no-install-recommends \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        libssl-dev \
        wget \
	nginx \
    && rm -rf /var/lib/apt/lists/*

ENV NVM_DIR /root/.nvm
ENV NODE_VERSION 12.18.3

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash \
    && mkdir -p /root/.nvm \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default \
    && cd /app \
    && npm i && npm run build

RUN rm -rf /app/node_modules

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/v$NODE_VERSION/bin:$PATH

# RUN apk update && apk add bash python 
# RUN apk add nginx openrc

COPY nginx/upstream.conf /etc/nginx/conf.d/upstream.conf
# COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.nginx /etc/nginx/sites-available/


EXPOSE 8080 1337
WORKDIR /app
CMD ["npm", "start"]
