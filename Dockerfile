FROM node:12-alpine

ENV NODE_ENV=production
ENV PATH /app/node_modules/.bin:$PATH

ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

RUN mkdir ./app
COPY ./institucional/package.json /app/

RUN cd /app && npm i

COPY ./institucional /app/
RUN cd /app && npm run build

EXPOSE 1337
WORKDIR /app
CMD ["npm", "start"]
