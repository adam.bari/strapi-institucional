# strapi institucional

docker run --name mysql_server -d -v mysql-volume:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=<password> -it -p 3306:3306 --rm mysql:5.7

docker exec -it mysql_server bash

aws ecr get-login-password --region us-east-1 --profile 652230569742_RestrictedAdmin | sudo docker login --username AWS --password-stdin 652230569742.dkr.ecr.us-east-1.amazonaws.com
sudo docker build -t strapi-institucional . --no-cache
sudo docker tag strapi-institucional:latest 652230569742.dkr.ecr.us-east-1.amazonaws.com/strapi-institucional:latest
sudo docker push 652230569742.dkr.ecr.us-east-1.amazonaws.com/strapi-institucional:latest
