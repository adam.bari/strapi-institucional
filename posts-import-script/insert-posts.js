const axios = require('axios')
const fs = require('fs')
const FormData = require('form-data')
const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjExMjY4NjUwLCJleHAiOjE2MTM4NjA2NTB9.cACStkOK04iwXl8okxZS8E4Q3wCsocapkrdIKSIwwQA'
const api = 'http://localhost:1337/'

// top level await
;(async () => {
  const uploadImage = async (imagePath) => {
    try {
      if (
        imagePath.substr(imagePath.length - 3) === 'psd' ||
        imagePath.substr(imagePath.length - 3) === 'ull' ||
        imagePath.includes('.null')
      ) {
        // console.log('will not upload' + imagePath)
        return null
      }
      // console.log('uploading ' + imagePath)
      const form_data = new FormData()
      form_data.append('files', fs.createReadStream('temp/' + imagePath))
      const config = {
        headers: {
          ...form_data.getHeaders(),
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
        maxContentLength: Infinity,
        maxBodyLength: Infinity,
      }
      const response = await axios.post(api + 'upload', form_data, config)
      console.log('image uploaded')
      return response.data
    } catch (error) {
      console.warn('image upload error for image:' + imagePath)
    }
  }

  const addCategory = async (slugToAdd) => {
    try {
      const response = await axios.get(api + 'categories/?slug=' + slugToAdd)
      if (response.data && response.data[0]) {
        return response.data[0].id
      }
      const category = await axios.post(
        api + 'categories',
        {
          name: slugToAdd,
          slug: slugToAdd,
        },
        { headers: { Authorization: `Bearer ${token}` } },
      )
      return category.data['id']
    } catch (e) {
      console.warn('cat error:', e.response.data)
    }
  }

  const upsertPost = async (post) => {
    let img = await uploadImage(post['image'] ? post['image'] : null)
    let storedPost = null
    let postId = null
    let response = null
    try {
      // console.log(
      //   '>> trying to get id of post ' + api + 'posts?slug=' + post['slug'],
      // )
      storedPost = await axios.get(api + 'posts?slug=' + post['slug'])
    } catch (e) {
      console.warn('Problem fetching post ' + post['slug'])
    }
    if (storedPost['data'] && storedPost['data'].length == 1) {
      // console.log('Post ID for ' + post['slug'] + ' found')
      postId = storedPost['data'][0]['id']
      // console.log('It is ' + postId)
      const postdata = {
        ...storedPost['data'][0],
        image: img && img[0] ? { id: img[0].id } : null,
      }
      try {
        response = await axios.put(
          api + 'content-manager/collection-types/application::post.post/' + postId,
          postdata,
          {
            headers: { Authorization: `Bearer ${token}` },
          },
        )
      } catch (e) {
        console.warn('Problem updating ' + post['slug'])
        console.warn(e)
      }
    } else {
      console.log('Post ID not found for ' + post['slug'])
      const postdata = {
        ...post,
        // categories: catsOnPost.map((c) => ({ id: c })),
        category: null,
        post_date: new Date(post.post_date).getTime(),
        image: img && img[0] ? { id: img[0].id } : null,
        _publicationState: 'preview',
      }
      console.log('Inserting post ' + postdata.slug)
      try {
        response = await axios.post(api + 'posts', postdata, {
          headers: { Authorization: `Bearer ${token}` },
        })
      } catch (e) {
        console.warn('Problem inserting ' + post['slug'])
      }
    }
  }

  const main = async () => {
    try {
      const posts = JSON.parse(
        fs.readFileSync('post-to-store.json', 'utf8').replace(/^\uFEFF/, ''),
      )
      let img = ''
      let response = null
      let post = null
      for (let p = 0; p < posts.length; p++) {
        await upsertPost(posts[p])
      }
    } catch (error) {
      console.log(error)
    }
  }
  main()
})()
