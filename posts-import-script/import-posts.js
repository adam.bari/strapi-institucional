const child_process = require('child_process')
const fs = require('fs')
const request = require('request')
const { v4: uuidv4 } = require('uuid')


function main(state) {
  let imagesToDownload = []
  console.log('Initializing Import Script')
  if (!fs.existsSync('./posts.json')) {
    throw 'posts.json file not found!'
  }
  if (!fs.existsSync('./images.json')) {
    throw 'images.json file not found!'
  }
  const images = JSON.parse(
    fs.readFileSync('images.json', 'utf8').replace(/^\uFEFF/, ''),
  )
    ['item'].filter(
      (image) => image['post_parent'] && image['post_parent']['__text'] !== '0',
    )
    .map((image) => {
      return {
        image: image['attachment_url']['__cdata'],
        post_id: image['post_parent']['__text'],
      }
    })
  const posts = JSON.parse(
    fs.readFileSync('posts.json', 'utf8').replace(/^\uFEFF/, ''),
  )['item'].map((post) => {
    if (!images.find((img) => img.post_id === post['post_id']['__text'])) {
      console.log('image not found for post ', post['post_id']['__text'])
    }
    const imageUrl = images.find(
      (img) => img.post_id === post['post_id']['__text'],
    )
      ? images.find((img) => img.post_id === post['post_id']['__text'])['image']
      : null
    const fileName = uuidv4() + '.' + (imageUrl ? imageUrl.split('.')[3] : null)
    if (imageUrl) {
      imagesToDownload.push({ url: imageUrl, fileName: './temp/' + fileName })
      // imageFile = await download(imageUrl, './temp/' + fileName, () => {
      //   console.log('image downloaded', imageUrl)
      // })
    }
    return {
      title: post['title'],
      content: post['encoded'][0]['__cdata'],
      post_date: post['pubDate'],
      post_id: post['post_id']['__text'],
      slug: post['post_name']['__cdata'],
      image: fileName,
      originalImgUrl: imageUrl,
      category: post['category'].map((c) => c['_nicename']),
    }
  })
  console.log(posts)
  const json = JSON.stringify(posts)
  fs.writeFile('./post-to-store.json', json, (err) => {
    if (!err) {
      console.log('done writing posts-to-store.json')
    }
  })
  fs.writeFile(
    './images-to-download.json',
    JSON.stringify(imagesToDownload),
    (err) => {
      if (!err) {
        console.log('done writing images-to-download.json')
      }
    },
  )
}

main()
