'use strict'

const axios = require('axios')
const fs = require('fs')
const Path = require('path')

async function downloadImage(url, fileName) {
  const path = Path.resolve(__dirname, './temp', fileName)
  const writer = fs.createWriteStream(path)
  const response = await axios({
    url: 'https://bancobari.com.br' + url,
    method: 'GET',
    responseType: 'stream',
  })
  response.data.pipe(writer)
  return new Promise((resolve, reject) => {
    writer.on('finish', resolve)
    writer.on('error', reject)
  })
}

;(async function () {
  console.log(1)
  const IMAGES = JSON.parse(
    fs.readFileSync('images-to-download.json', 'utf8').replace(/^\uFEFF/, ''),
  )
  console.log(IMAGES.length)
  for (let i = 0; IMAGES.length > i; i++) {
    let image = IMAGES[i]
    console.log(image)
    if (!image.url.includes('.psd'))
      downloadImage(image.url, image.fileName)
    console.log(image.url)
  }
  console.log('the end')
})()
